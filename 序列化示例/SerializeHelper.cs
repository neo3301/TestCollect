﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace SerializeHelper
{
    //序列化就类似保存到本地  反序列化就是加载读取
    //[Obsolete]
    //class SerializeUtil
    //{

    //    //  也就是说我们在使用XmlSerializer序列化，初始化XmlSerializer对象时最好使用下面两个构造函数否则会引起内存泄漏。
    //    //XmlSerializer(Type)
    //    //XmlSerializer.XmlSerializer(Type, String)
    //    /// <summary>
    //    /// 将XML字符串反序列化成对象
    //    /// </summary>
    //    /// <typeparam name="T">返回的对象类型</typeparam>
    //    /// <typeparam name="xml">xml字符串</typeparam>
    //    /// <typeparam name="xmlRootName">xml根节点</typeparam>
    //    /// <returns>返回对象类型</returns>
    //    public static T Deserialize<T>(string xml, string xmlRootName = "Root")
    //    {
    //        T result = default(T);
    //        using (StringReader sr = new StringReader(xml))
    //        {
    //            XmlSerializer xmlSerializer = string.IsNullOrWhiteSpace(xmlRootName) ?
    //                new XmlSerializer(typeof(T)) : new XmlSerializer(typeof(T), new XmlRootAttribute(xmlRootName));
    //            result = (T)xmlSerializer.Deserialize(sr);
    //        }
    //        return result;
    //    }

    //    /// <summary>
    //    /// 将xml文件转换为对象
    //    /// </summary>
    //    /// <typeparam name="T"></typeparam>
    //    /// <param name="filePath"></param>
    //    /// <param name="xmlRootName"></param>
    //    /// <returns></returns>
    //    public static T DeserializeFile<T>(string filePath, string xmlRootName = "Root")
    //    {
    //        T result = default(T);
    //        if (File.Exists(filePath))
    //        {
    //            using (StreamReader reader = new StreamReader(filePath))
    //            {
    //                //三元表达式
    //                XmlSerializer xmlSerializer = string.IsNullOrWhiteSpace(xmlRootName) ?
    //                    new XmlSerializer(typeof(T)) : new XmlSerializer(typeof(T), new XmlRootAttribute(xmlRootName));
    //                result = (T)xmlSerializer.Deserialize(reader);
    //            }
    //        }
         
    //        return result;
    //    }
    //    /// <summary>
    //    /// 将对象序列化成XML字符串
    //    /// </summary>
    //    /// <typeparam name="sourceObj">数据对象</typeparam>
    //    /// <typeparam name="xmlRootName">xml根节点 默认值</typeparam>
    //    /// <returns>xml字符串</returns>
    //    public static string Serializer(object sourceObj, string xmlRootName = "Root")
    //    {
    //        MemoryStream Stream = new MemoryStream();
    //        Type type = sourceObj.GetType();
    //        XmlSerializer xmlSerializer = string.IsNullOrWhiteSpace(xmlRootName) ?
    //            new XmlSerializer(type) : new XmlSerializer(type, new XmlRootAttribute(xmlRootName));
    //        xmlSerializer.Serialize(Stream, sourceObj);
    //        Stream.Position = 0;
    //        StreamReader sr = new StreamReader(Stream);
    //        string str = sr.ReadToEnd();
    //        sr.Dispose();
    //        Stream.Dispose();

    //        return str;
    //    }
    //    /// <summary>
    //    /// 将对象序列化成XML文件
    //    /// </summary>
    //    /// <typeparam name="filePath">xml文件路径</typeparam>
    //    /// <typeparam name="sourceObj">数据对象</typeparam>
    //    /// <typeparam name="xmlRootName">xml根节点</typeparam>
    //    public static void SerializerFile(string filePath, object sourceObj, string xmlRootName = "Root")
    //    {
    //        if (!string.IsNullOrWhiteSpace(filePath) && sourceObj != null)
    //        {
    //            Type type = sourceObj.GetType();
    //            using (StreamWriter writer = new StreamWriter(filePath))
    //            {
    //                XmlSerializer xmlSerializer = string.IsNullOrWhiteSpace(xmlRootName) ?
    //                    new XmlSerializer(type) : new XmlSerializer(type, new XmlRootAttribute(xmlRootName));

    //                xmlSerializer.Serialize(writer, sourceObj);
    //            }
    //        }
    //    }
    //}


    /// <
    /// summary>
    /// 这个类里包含了xml json bin三种类型的序列化与反序列化
    /// </summary>
    public class SerializeHelper
    {
        /// <summary>
        /// 序列化为对象
        /// </summary>
        /// <param name="objname"></param>
        /// <param name="obj"></param>
        public static void BinarySerialize(string objname, object obj)
        {
            try
            {
                string filename = objname + ".Binary";
                //if (System.IO.File.Exists(filename))
                //    System.IO.File.Delete(filename);
                using (FileStream fileStream = new FileStream(filename, FileMode.Create))
                {
                    // 用二进制格式序列化
                    BinaryFormatter binaryFormatter = new BinaryFormatter();
                    binaryFormatter.Serialize(fileStream, obj);
                    fileStream.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// 从二进制文件中反序列化
        /// </summary>
        /// <param name="objname"></param>
        /// <returns></returns>
        public static object BinaryDeserialize(string objname)
        {
            System.Runtime.Serialization.IFormatter formatter = new BinaryFormatter();
            //二进制格式反序列化
            object obj;
            string filename = objname + ".Binary";
            if (!System.IO.File.Exists(filename))
                throw new Exception("在反序列化之前,请先序列化");
            using (Stream stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                obj = formatter.Deserialize(stream);
                stream.Close();
            }
            //using (FileStream fs = new FileStream(filename, FileMode.Open))
            //{
            //    BinaryFormatter formatter = new BinaryFormatter();
            //    object obj = formatter.Deserialize(fs);
            //}
            return obj;

        }

        /// <summary>
        /// 泛型保存 与下面的区别是少了个参数检测
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="obj"></param>
        public static void XmlSerialize(string filename, object obj)
        {

            try
            {
                //string filename = objname + ".xml";
                //if (System.IO.File.Exists(filename))
                //    System.IO.File.Delete(filename);
                using (FileStream fileStream = new FileStream(filename, FileMode.Create))
                {
                    Type type = obj.GetType();
                    // 序列化为xml
                    //XmlSerializer formatter = new XmlSerializer(typeof(Car));
                    XmlSerializer xmlSerializer = new XmlSerializer(type);

                    xmlSerializer.Serialize(fileStream, obj);
                    fileStream.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        /// <summary>
        /// 泛型序列化
        /// </summary>
        /// <param name="filePath">需要带文件类型后缀</param>
        /// <param name="sourceObj">需要序列化的对象</param>
        /// <param name="xmlRootName">可选参数</param>
        public static void GenericSerializerFile(string filePath, object sourceObj, string xmlRootName = "Root")
        {
            if (!string.IsNullOrWhiteSpace(filePath) && sourceObj != null)
            {
                Type type = sourceObj.GetType();
                using (StreamWriter writer = new StreamWriter(filePath))
                {
                    XmlSerializer xmlSerializer = string.IsNullOrWhiteSpace(xmlRootName) ?
                        new XmlSerializer(type) : new XmlSerializer(type, new XmlRootAttribute(xmlRootName));

                    xmlSerializer.Serialize(writer, sourceObj);
                }
            }
        }

        /// <summary>
        /// 从xml序列中反序列化
        /// </summary>
        /// <param name="objname"></param>
        /// <returns></returns>
        public static object XmlDeserailize<T>(string objname)
        {
            // System.Runtime.Serialization.IFormatter formatter = new XmlSerializer(typeof(Car));
            string filename = objname + ".xml";
            object obj;
            if (!System.IO.File.Exists(filename))
                throw new Exception("对反序列化之前,请先序列化");
            //Xml格式反序列化
            using (Stream stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                XmlSerializer formatter = new XmlSerializer(typeof(T));
                obj = (T)formatter.Deserialize(stream);
                stream.Close();
            }
            return obj;
        }


        #region Json序列化
        /// <summary>
        /// JsonSerializer序列化
        /// </summary>
        /// <param name="item">对象</param>
        public static string ToJson<T>(T item)
        {
            var serializer = JsonConvert.SerializeObject(item.GetType());
           
            return serializer;
        }

        /// <summary>
        /// JsonSerializer反序列化
        /// </summary>
        /// <param name="str">字符串序列</param>
        public static T FromJson<T>(string str) where T : class
        {
            T res = JsonConvert.DeserializeObject<T>(str);
            return res;
            
        }
        #endregion
    }

}
