﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace SerializeHelper
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //XmlHelper xmlHelper;
        private void button1_Click(object sender, EventArgs e)
        {
            //Request q = new Request();
            //q.ReqType = "connection.getStatus";
            //q.NameID = "8489237";

            //Telegram telegram = new Telegram();
            //telegram.Requestins = q;
            //var res = Object2Bytes(telegram);
            //var res = XmlHelper.DeserializeFile<BookRoot>(@"D:\CODING\GitRespository\Csharp\TestDemoCollect\测试.xml", "BookRoot");

            //this.propertyGrid1.SelectedObject = 
            SerializeHelperDemo();
            //SerializeUtilDemo();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            this.propertyGrid1.Refresh();
            SerializeHelper.GenericSerializerFile("car.xml", this.propertyGrid1.SelectedObject, "cfg");

        }
        public byte[] Object2Bytes(object obj)
        {
            byte[] buff;
            using (MemoryStream ms = new MemoryStream())
            {
                IFormatter formatter = new BinaryFormatter();
                formatter.Serialize(ms, obj);
                buff = ms.GetBuffer();
            }
            return buff;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.propertyGrid1.SelectedObject = new Cfg();
            //Console.WriteLine(Power(2,2));
            foreach (int i in Power(2, 8))
            {
                Console.WriteLine("{0} ", i);
            }
        }
        public static IEnumerable<int> Power(int number, int exponent)
        {
            int result = 1;

            for (int i = 0; i < exponent; i++)
            {
                result = result * number;
                yield return result;
            }
        }
        public static int Power2(int number, int exponent)
        {
            int result = 1;

            for (int i = 0; i < exponent; i++)
            {
                result = result * number;
                //return result;
            }
            return result;
        }

        public void SerializeHelperDemo()
        {
            Car carins = new Car("neo","100w","bmw");
            string json3 = @"{'Name': '张三','Age': '23'}";
            var htmlAttributes = JsonConvert.DeserializeObject<Dictionary<string, string>>(json3);

            Console.WriteLine(htmlAttributes);
            SerializeHelper.BinarySerialize("Binary序列化", carins);
            //Serialize.SoapSerialize("Soap序列化", car);
            SerializeHelper.XmlSerialize("XML序列化", carins);
            //反序列化
            Car car2 = (Car)SerializeHelper.BinaryDeserialize("Binary序列化");
            //car2 = (Car)Serialize.SoapDeserialize("Soap序列化");
            car2 = (Car)SerializeHelper.XmlDeserailize<Car>("XML序列化");
            this.propertyGrid1.SelectedObject = car2;
        }
        //public void SerializeUtilDemo()
        //{
        //    var res = SerializeUtil.DeserializeFile<Cfg>(@"D:\CODING\GitRespository\Csharp\TestDemoCollect\cfg.xml", "cfg");

        //    SerializeUtil.Serializer(res, "BookRoot");
        //    SerializeUtil.SerializerFile("test.xml", res, "cfg");
        //    this.propertyGrid1.SelectedObject = res;
        //}

       
    }





    public class Cfg
    {
        //关于这个类，如果是想在grid中的第一层显示，那就需要将device类展开为具体类，下面的不在是list,而是每个具体的device
        [XmlElement("device")]
        public List<Device> devList { get; set; }

    }
    public class Device
    {
        //优先查找 xmlelement 对应的节点名称 如果没有设置就按属性名称查找
        [XmlElement("设置1")]
        [Category("setting")]
        public string setting1 { get; set; } = "1";
        [XmlElement("设置2")]

        public string setting2 { get; set; }
        [XmlElement("设置3")]

        public string setting3 { get; set; }
        [XmlElement("设置4")]

        public string setting4 { get; set; }
    }


    


    #region book

    public class BookDetail
    {
        /// <summary>
        /// 李刚
        /// </summary>
        [XmlElement(IsNullable = false)]
        //[XmlIgnoreAttribute]
        public string author { get; set; } = "战胜";

        /// <summary>
        /// 疯狂XML讲义
        /// </summary>
        [XmlElement]
        public string title { get; set; } = "名字";

        /// <summary>
        /// 电子工业出版社
        /// </summary>
        [XmlElement(IsNullable = false)]

        public string publisher { get; set; } = "出版社名字";

    }
    public class BookRoot
    {
        /// <summary>
        /// BookDetail
        /// </summary>
        [XmlElementAttribute(IsNullable = false)]

        public List<BookDetail> BookDetail { get; set; }

    }
    [XmlRoot(IsNullable = false)]
    public class Root
    {
        /// <summary>
        /// BookRoot
        /// </summary>
        public BookRoot BookRoot { get; set; }

    }


    #endregion



    #region 要序列化的类
    [Serializable]
    public class Car
    {
        private string _Price;
        private string _Owner;
        private string m_filename;

        [XmlElement(ElementName = "Price")]
        public string Price
        {
            get { return this._Price; }
            set { this._Price = value; }
        }

        [XmlElement(ElementName = "Owner")]
        public string Owner
        {
            get { return this._Owner; }
            set { this._Owner = value; }
        }

        [XmlElement(ElementName = "品牌")]
        public string CarBrand { get; set; }

        public string Filename
        {
            get
            {
                return m_filename;
            }
            set
            {
                m_filename = value;
            }
        }

        public Car(string o, string p,string b)
        {
            this.Price = p;
            this.Owner = o;
            this.CarBrand = b;
        }

        public Car()
        {

        }
    }
    #endregion


}
