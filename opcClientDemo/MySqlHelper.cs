﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace opcClientDemo
{
    class MySqlHelper
    {
        static IFreeSql fsql;
        public MySqlHelper()
        {
             fsql = new FreeSql.FreeSqlBuilder()
                
                                       //.UseConnectionString(FreeSql.DataType.Sqlite, "data source=test.db")
                                       .UseConnectionString(FreeSql.DataType.MySql, "Data Source=127.0.0.1;Port=3306;User ID=root;Password=root;Initial Catalog=testdemo;Charset=utf8")
                                       //.UseMonitorCommand(cmd => Trace.WriteLine($"线程：{cmd.CommandText}\r\n"))
                                       .UseAutoSyncStructure(true) //自动创建、迁移实体表结构
                                       .UseNoneCommandParameter(true)
                                       .Build();
        }

        public bool SaveRes2DB(string id,string value )
        {
            bool status = false;
            try
            {
            Device device = new Device();
            device.CreateTime = DateTime.Now;
            device.DeviceName = id;
            device.StrDeviceValue = value;
            long res= fsql.Insert<Device>().AppendData(device).ExecuteIdentity();
                status = res > 0 ? true:false;
            }
            catch (Exception ex)
            {
                status = false;
                throw ex;
            }
            return status;
            //Console.WriteLine(res);
        }


    } 

    class Device
    {
        [Column(IsIdentity = true, IsPrimary = true)]
        public int ID { get; set; }
        public string DeviceName { get; set; }
        //public object ShortDeviceValue { get; set; }
        public string StrDeviceValue { get; set; }
        //public double DoubDeviceValue { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
