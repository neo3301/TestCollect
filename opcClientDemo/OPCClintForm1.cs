﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Opc.Ua;
using Opc.Ua.Client;
using OpcUaHelper;

namespace opcClientDemo
{
    public partial class FrmOpcClient : Form
    {
        OpcUaClient client;
        FrmOpcClient FrmOpc;
        MySqlHelper sqlHelper;
        public FrmOpcClient()
        {

            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Init();
            richTextBox1.Focus();
            richTextBox1.SelectionStart = richTextBox1.Text.Length;
            richTextBox1.ScrollToCaret();
            //richTextBox1.AutoScrollOffset
        }
        public void Init()
        {
            client = new OpcUaClient();
            sqlHelper = new MySqlHelper();

        }
        CancellationToken token;
        CancellationTokenSource tokenSource;
        ManualResetEvent resetEvent = new ManualResetEvent(true); //true  默认waitone是通的，set 使通，reset使阻止
        Action<string, DataValue> ShowLog;
        async private void button1_Click(object sender, EventArgs e)

        {
            client.UserIdentity = new UserIdentity(new AnonymousIdentityToken());
            tokenSource = new CancellationTokenSource();
            token = tokenSource.Token;

            //client.UserIdentity = new UserIdentity("sysadmin", "demo");
            string url = @"opc.tcp://127.0.0.1:49320";
            //string url = @"opc.tcp://127.0.0.1:62547/DataAccessServer";
            await client.ConnectServer(url);

            //string id = @"ns=2;s=通道 1.demodevice2._System._DeviceId";
            //string id = @"ns=2;s=数据类型示例.16 位设备.R 寄存器.LLong4";
            //ns = 2; s = 通道 1.demodevice2.组1  //此处的名称的空格不能少 默认命名方式有空格
            //string id = @"ns=2;s=通道1.demodevice2.组1.类1";
            string id = @"ns=2;s=DataAccessServer.设备 1.com1";
            var res = client.ReadNode(id);
            //var res1 = client.ReadNodes(new NodeId[] { "ns=2;s=通道 1.demodevice2._System" });
            richTextBox1.AppendText(id + "对应的值为：" + res + Environment.NewLine);

            ReferenceDescriptionCollection references = GetReferenceDescriptionCollection("ns=2;s=通道1.demodevice2.组1.类1");
            //ShowLog= new Action<string, DataValue>(UpdateLogV2)
            //WriteInt();
            ShowLog += UpdateLogV2;

            foreach (var item in references)
            {
                richTextBox1.AppendText(item.ToString() + Environment.NewLine);
            }

            Task task1 = new Task(Read1);
            new Task(Read1).Start();
            new Task(Read1).Start();
            new Task(Read1).Start();
            new Task(Read1).Start();


            Task task2 = new Task(Read2);
            Task task3 = new Task(Read2);
            Task task4 = new Task(Read2);
            //task1.Start();
            //task2.Start();
            //task3.Start();
            //task4.Start();
            //var  task5 = new Task(DeadTH);
            //task5.Start();
            client.OpcStatusChange += UpdateopcStatus;
        }

        //private void UpdateopcStatus(object sender, EventArgs e)

        private void UpdateopcStatus(object sender, OpcUaStatusEventArgs e)
        {
            //throw new NotImplementedException();
            if (e.Error)
            {
                this.Invoke(new Action(() =>
                    label1.Text = e.Text));
            }
            else
            {
                this.Invoke(new Action(() =>
                 label1.Text = DateTime.Now.ToString("hh:mm:ss.ff")));
            }
        }


        private void DeadTH()
        {
            while (true)
            {
                Console.WriteLine(DateTime.Now.ToString());
            }
        }

        private async void Read1()
        {



            string id1 = @"ns=2;s=通道1.demodevice2.组1.Int寄存器";

            while (true)
            {
                try
                {
                    if (token.IsCancellationRequested)
                {
                    return;
                }
                     var res = client.ReadNode(id1);

                    //var res1 = client.ReadNodes(new NodeId[] { "ns=2;s=通道 1.demodevice2._System" });
                    //richTextBox1.AppendText(id + "对应的值为：" + res + Environment.NewLine);
                    //UpdateLog(id1, res);

                    //Thread.Sleep(20);


                    //if (isShow)
                    //{ 
                    //    BeginInvoke(ShowLog, DateTime.Now.ToString("hh:mm:ss.ff") + client.Connected.ToString()+ ":" + id1, res); 
                    //}
                    //else break;

                    //update db
                
                    if (!sqlHelper.SaveRes2DB(id1, res.Value.ToString()))
                    {
                        Console.WriteLine("sql in error");
                    }
                    //if (!isShow) { break; }
                    resetEvent.WaitOne();
                    await Task.Delay(200);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("sql in e" +ex.Message);
                    //throw;
                }
            }

        }

        private async void Read2()
        {
            string id = @"ns=2;s=数据类型示例.16 位设备.R 寄存器.LLong4";

            //string id = @"ns=2;s=通道 1.demodevice2._System._DeviceId";
            while (true)
            {
                if (token.IsCancellationRequested)
                {
                    return;
                }
                var res = client.ReadNode(id);
                //var res1 = client.ReadNodes(new NodeId[] { "ns=2;s=通道 1.demodevice2._System" });

                //if (isWindowShow) Invoke(showInfo, data.ToString());
                //UpdateLog(id, res);
                //Invoke( new Action(()=>UpdateLog(id,res)));

                //Thread.Sleep(20);


                if (isShow)
                {
                    BeginInvoke(ShowLog, DateTime.Now.ToString("hh:mm:ss.ff") + ":" + id, res);
                }
                else break;
                if (!isShow) { break; }
                resetEvent.WaitOne();
                await Task.Delay(200);
            }
        }


        private async void WriteInt()
        {
            string id = @"ns=2;s=通道1.demodevice2.组1.类1";
            string id2 = @"ns=2;s=通道1.demodevice2.组1.int寄存器";
            string id3 = @"ns=2;s=通道1.demodevice2.组1.string寄存器";
            //string id = @"ns=2;s=通道 1.demodevice2._System._DeviceId";
            while (true)
            {
                if (token.IsCancellationRequested)
                {
                    return;
                }
                //var res = client.WriteNode<double>(id,11.2);
                var res = client.WriteNode<double>(id, 11.2);
                var res1 = client.WriteNode<Int16>(id2, 11);
                var res2 = client.WriteNode<string>(id3, "sssg");
                if (res)
                {
                    var res11 = client.ReadNode<double>(id);
                    var res21 = client.ReadNode<Int16>(id2);
                    var res31 = client.ReadNode<string>(id3);
                    BeginInvoke(ShowLog, DateTime.Now.ToString("hh:mm:ss.ff") + ":" + id, res1);
                }
                else break;
                if (!isShow) { break; }
                resetEvent.WaitOne();
                await Task.Delay(100);
            }
        }
        private void UpdateLog(string id, DataValue res)
        {
            //if (this.IsDisposed||client==null) { return; }

            if (richTextBox1.InvokeRequired)
            {
                this.Invoke(new Action(() =>
                richTextBox1?.AppendText(DateTime.Now.ToString("HH:mm:ss.fffff") + id + " " + "对应的值为：" + res + Environment.NewLine)
                ));
            }
        }
        private void UpdateLogV2(string idtamp, DataValue res)
        {
            //label1.Text = DateTime.Now.ToString("HH:mm:ss.fffff") + id;
            richTextBox1?.AppendText(idtamp + res + Environment.NewLine);
        }
        private ReferenceDescriptionCollection GetReferenceDescriptionCollection(NodeId sourceId)
        {
            TaskCompletionSource<ReferenceDescriptionCollection> task = new TaskCompletionSource<ReferenceDescriptionCollection>();

            // find all of the components of the node.
            BrowseDescription nodeToBrowse1 = new BrowseDescription();

            nodeToBrowse1.NodeId = sourceId;
            nodeToBrowse1.BrowseDirection = BrowseDirection.Forward;
            nodeToBrowse1.ReferenceTypeId = ReferenceTypeIds.Aggregates;
            nodeToBrowse1.IncludeSubtypes = true;
            nodeToBrowse1.NodeClassMask = (uint)(NodeClass.Object | NodeClass.Variable | NodeClass.Method | NodeClass.ReferenceType | NodeClass.ObjectType | NodeClass.View | NodeClass.VariableType | NodeClass.DataType);
            nodeToBrowse1.ResultMask = (uint)BrowseResultMask.All;

            // find all nodes organized by the node.
            BrowseDescription nodeToBrowse2 = new BrowseDescription();

            nodeToBrowse2.NodeId = sourceId;
            nodeToBrowse2.BrowseDirection = BrowseDirection.Forward;
            nodeToBrowse2.ReferenceTypeId = ReferenceTypeIds.Organizes;
            nodeToBrowse2.IncludeSubtypes = true;
            nodeToBrowse2.NodeClassMask = (uint)(NodeClass.Object | NodeClass.Variable | NodeClass.Method | NodeClass.View | NodeClass.ReferenceType | NodeClass.ObjectType | NodeClass.VariableType | NodeClass.DataType);
            nodeToBrowse2.ResultMask = (uint)BrowseResultMask.All;

            BrowseDescriptionCollection nodesToBrowse = new BrowseDescriptionCollection();
            nodesToBrowse.Add(nodeToBrowse1);
            nodesToBrowse.Add(nodeToBrowse2);

            // fetch references from the server.
            ReferenceDescriptionCollection references = FormUtils.Browse(client.Session, nodesToBrowse, false);
            return references;
        }

        bool isShow = false;
        private void OpcClient_FormClosing(object sender, FormClosingEventArgs e)
        {
            isShow = false;
            tokenSource?.Cancel();

            //System.Environment.Exit(0);

        }
        bool clickedOnce = true;
        private void btnStop_Click(object sender, EventArgs e)
        {

            if (clickedOnce)
            {
                //stop = false;
                resetEvent.Reset();
                //tokenSource.Cancel();'
                clickedOnce = false;
                btnStop.Text = "继续";
            }
            else
            {
                //stop = true;
                resetEvent.Set();
                clickedOnce = true;
                btnStop.Text = "暂停";
            }
        }
        //public void Meth()
        //{
        //    public ReferenceDescriptionCollection BrowseNodes(NodeId sourceId)
        //    {

        //        m_session = this.MySession;

        //        if (m_session == null)
        //        {
        //            return null;
        //        }

        //        // set a suitable initial state.
        //        //if (m_session != null && !m_connectedOnce)
        //        //{
        //        //    m_connectedOnce = true;
        //        //}

        //        // fetch references from the server.
        //        // find all of the components of the node.
        //        BrowseDescription nodeToBrowse1 = new BrowseDescription();

        //        nodeToBrowse1.NodeId = sourceId;
        //        nodeToBrowse1.BrowseDirection = BrowseDirection.Forward;
        //        nodeToBrowse1.ReferenceTypeId = ReferenceTypeIds.Aggregates;
        //        nodeToBrowse1.IncludeSubtypes = true;
        //        nodeToBrowse1.NodeClassMask = (uint)(NodeClass.Object | NodeClass.Variable);
        //        nodeToBrowse1.ResultMask = (uint)BrowseResultMask.All;

        //        Browser bro = new Browser(m_session);
        //        ReferenceDescriptionCollection references = bro.Browse(sourceId);
        //        return references;
        //    }
        //}
    }
}
