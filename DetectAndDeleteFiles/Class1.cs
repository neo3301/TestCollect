﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DetectAndDeleteFiles
{
    /// <summary>
    /// 删除文件或者文件夹，按照创建日期来，后续使用的时候，可以把参数部分提取出来放到属性类中，配置到配置文件里
    /// </summary>
    public class FileManager
    {
        //public string fileDirectory { get; set; }
        public string fileDirectory { get; set; }
        public int saveDay { get; set; } = 60;

        System.Timers.Timer aTimer;

        public void AutoDeletTimer()
        {
            if (aTimer == null) { aTimer = new System.Timers.Timer(); }
            aTimer.Elapsed += OnTimer_tick;
            aTimer.AutoReset = true;
            aTimer.Interval = 100 * 60 * 60;
            aTimer.Enabled = true;
            //aTimer.Start();== enable
        }
        private void OnTimer_tick(Object source, System.Timers.ElapsedEventArgs e)
        {
            if (!DeleteFile())
            {
                aTimer.Stop();
                //aTimer.Enabled = false;
            }
            if (!DeleteDirectory())
            {
                aTimer.Stop();
                //aTimer.Enabled = false;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="str_HardDiskName"></param>
        /// <returns></returns>
        public long GetHardDiskSpace(string str_HardDiskName)
        {
            long totalSize = new long();
            str_HardDiskName = str_HardDiskName.ToUpper() + ":\\";

            System.IO.DriveInfo[] drives = System.IO.DriveInfo.GetDrives();

            foreach (System.IO.DriveInfo drive in drives)
            {
                if (drive.Name == str_HardDiskName)
                {
                    totalSize = drive.TotalSize / (1024 * 1024 * 1024);
                }
            }
            return totalSize;
        }

        /// <summary>
        /// 获取指定驱动器的剩余空间总大小(单位为GB)  
        /// </summary>
        /// <param name="str_HardDiskName">只需输入代表驱动器的字母即可,不区分大小写</param>
        /// <returns></returns>
        public long GetHardDiskFreeSpace(string str_HardDiskName)
        {
            long freeSpace = new long();
            str_HardDiskName = str_HardDiskName.ToUpper() + ":\\";
            DriveInfo[] drives = DriveInfo.GetDrives();
            foreach (System.IO.DriveInfo drive in drives)
            {

                if (drive.Name == str_HardDiskName)
                {
                    freeSpace = drive.TotalFreeSpace / (1024 * 1024 * 1024);
                }
            }
            return freeSpace;
        }

        /// <summary>
        /// 删除文件夹 删除最先创建的文件
        /// </summary>
        /// <param name="filePath">要删除的文件所在的文件夹</param>
        /// <param name="saveDay">最长保存时间</param>
        public bool DeleteFile()
        {
            if (fileDirectory == null) { Console.WriteLine("文件为空"); return false; }
            try
            {
                DateTime nowTime = DateTime.Now;
                DirectoryInfo root = new DirectoryInfo(fileDirectory);
                //DirectoryInfo[] dics = root.GetDirectories();//获取文件夹
                FileInfo[] dics = root.GetFiles();
                FileAttributes attr = File.GetAttributes(fileDirectory);
                //if (attr == FileAttributes.)//判断是不是文件夹  这是删除子文件夹
                //{
                foreach (FileInfo file in dics)//遍历文件夹
                {
                    TimeSpan t = nowTime - file.CreationTime;  //当前时间  减去 文件创建时间
                    int day = t.Days;
                    if (day > saveDay)   //保存的时间 ；  单位：天
                    {
                        //Directory.Delete(file.FullName, true);  //删除超过时间的文件夹
                        File.Delete(file.FullName);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                //MessageBox.Show("删除异常" + ex.Message);
                return false;
            }
            //}
        }
        /// <summary>
        /// 删除文件夹下的子目录
        /// </summary>
        /// <param name="fileDirectory"></param>
        /// <param name="saveDay"></param>
        public bool DeleteDirectory()
        {
            if (fileDirectory == null) { Console.WriteLine("路径为空"); return false; }
            try
            {
                DateTime nowTime = DateTime.Now;
                DirectoryInfo root = new DirectoryInfo(fileDirectory);
                DirectoryInfo[] dics = root.GetDirectories();//获取文件夹
                FileAttributes attr = File.GetAttributes(fileDirectory);
                if (attr == FileAttributes.Directory)//判断是不是文件夹  这是删除子文件夹
                {
                    foreach (DirectoryInfo file in dics)//遍历文件夹
                    {
                        TimeSpan t = nowTime - file.CreationTime;  //当前时间  减去 文件创建时间
                        int day = t.Days;
                        if (day > saveDay)   //保存的时间 ；  单位：天
                        {
                            Directory.Delete(file.FullName, true);  //删除超过时间的文件夹
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                //实际使用中此处的异常应该直接throw
                //MessageBox.Show("删除异常" + ex.Message);
                return false;

            }
        }



    }
}
