﻿using OpcUaHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace opcServerDemo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            thread = new System.Threading.Thread(new System.Threading.ThreadStart(ThreadCapture));
            thread.IsBackground = true;
            thread.Start();
        }
        //测试线程循环invoke，关闭时的释放问题
        private void ThreadCapture()
        {
            showInfo = new Action<string>(m =>
            {
                label1.Text = m;
            });
            isWindowShow = true;

            System.Threading.Thread.Sleep(200);
            while (true)
            {
                // 我们假设这个数据是从PLC或是远程服务器获取到的，因为可能比较耗时，我们放在了后台线程获取，并且处于一直运行的状态
                // 我们还假设获取数据的频率是200ms一次，然后把数据显示出来
                int data = random.Next(1000);

                // 接下来是跨线程的显示，并检测窗体是否关闭
                if (isWindowShow) BeginInvoke(showInfo, DateTime.Now.ToString("hh:mm:ss.ff"));
                else break;


                System.Threading.Thread.Sleep(20);
                // 再次检测窗体是否关闭
                if (!isWindowShow) break;
            }

            // 通知主界面是否准备退出
            //resetEvent.Set();
        }


        private System.Threading.AutoResetEvent resetEvent = new System.Threading.AutoResetEvent(false);
        private bool isWindowShow = false;
        private Action<string> showInfo;
        private System.Threading.Thread thread;
        private Random random = new Random();

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            isWindowShow = false;
            //resetEvent.WaitOne();  //阻止关闭，知道更新完成
        }

        private OpcUaClient m_OpcUaClient;

        private void button4_Click(object sender, EventArgs e)
        {
            // sub

            //m_OpcUaClient.AddSubscription("A", "ns=2;s=Machines/Machine B/TestValueFloat", SubCallback);

        }
        OpcUaClient client = new OpcUaClient();
        private async void button1_Click(object sender, EventArgs e)
        {
            string url = @"opc.tcp://127.0.0.1:49320";

            await client.ConnectServer(url);


        }

        private async void Read1()
        {
            string id1 = @"ns=2;s=通道1.demodevice2.组1.类1";
            bool isShow = true;
            //while (true)
            //{
                //if (token.IsCancellationRequested)
                //{
                //    return;
                //}
                var res = client.ReadNode(id1);

                //var res1 = client.ReadNodes(new NodeId[] { "ns=2;s=通道 1.demodevice2._System" });
                //richTextBox1.AppendText(id + "对应的值为：" + res + Environment.NewLine);
                //UpdateLog(id1, res);

                //Thread.Sleep(20);


                //if (isShow)
                //{
                //    BeginInvoke(ShowLog, DateTime.Now.ToString("hh:mm:ss.ff") + client.Connected.ToString() + ":" + id1, res);
                //}
                //else break;
                //if (!isShow) { break; }
                //resetEvent.WaitOne();
                //await Task.Delay(100);

            }

        private void button2_Click(object sender, EventArgs e)
        {
            Read1();
        }
    
        /*
        private void SubCallback(string key, MonitoredItem monitoredItem, MonitoredItemNotificationEventArgs args)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<string, MonitoredItem, MonitoredItemNotificationEventArgs>(SubCallback), key, monitoredItem, args);
                return;
            }

            if (key == "A")
            {
                // 如果有多个的订阅值都关联了当前的方法，可以通过key和monitoredItem来区分
                MonitoredItemNotification notification = args.NotificationValue as MonitoredItemNotification;
                if (notification != null)
                {
                    textBox1.Text = notification.Value.WrappedValue.Value.ToString();
                }
            }

        }
        */

    }
}
