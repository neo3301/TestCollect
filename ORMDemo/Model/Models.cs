﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreeSql.DataAnnotations;

namespace ORMDemo
{
    //freesql 等orm工具，当用此等工具时，默认的表名称与class是对应的大小写及单复数形式
    public class Users
    {
        [Column(IsIdentity = true, IsPrimary = true)]
        public int ID { get; set; }
        public string Name { get; set; }
        public string PassWord { get; set; }
        public DateTime Create_time { get; set; }
    }
}
