﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static 关于接口与拆装箱.IPersonName;

namespace 关于接口与拆装箱
{

    /*
     * 这个测试让我很疑惑
     * 为什么用接口减少了拆装箱操作 却还是比强制类型转换并执行拆装箱操作要费时，30ticks vs 90ticks
     */
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            //Method();
            //MethodInterface();
            Method3();
        }
        public void Method()
        {
            // Using the person in a collection

            ArrayList arrPersons = new ArrayList();

            Person p = new Person();

            arrPersons.Add(p);



            // Try to change the name

            p = (Person)arrPersons[0];

            p.Name = "NewName";



            Debug.WriteLine(((Person)arrPersons[0]).Name);//It's "OldName"



            //这个问题其实在前面的文章中已经讲过了。有人可能会说，是否可以按照如下的方式去修改呢。

            //((Person)arrPersons[0]).Name = "NewName";//Can't be compiled



            //很不幸，如上操作不能通过编译。为什么呢，对于“((Person)arrPersons[0])”来说，是系统用一个临时变量来接收拆箱后的值类型数据，那么由于值类型是分配在栈上，那么操作是对实体操作，可是系统不允许对一个临时值类型数据进行修改操作。

            // Using the person in a collection

            ArrayList arrPersons1 = new ArrayList();

            Person p1 = new Person();

            arrPersons1.Add(p1);



            // Try to change the name

            p1 = (Person)arrPersons1[0];

            p1.Name = "NewName";



            arrPersons1.RemoveAt(0);//Remove old data first

            arrPersons1.Insert(0, p);//Add new data

            Debug.WriteLine(((Person)arrPersons1[0]).Name);//It's "NewName"


        }
        public void MethodInterface()
        {
            // Using the person in a collection

            ArrayList arrPersons = new ArrayList();

            Person p = new Person();

            arrPersons.Add(p);



            // Change the name

            //((IPersonName)arrPersons[0]).Name = "NewName";
            //((Person)arrPersons[0]).Name = "";


            Debug.WriteLine(((Person)arrPersons[0]).Name);//It's "NewName"

        }

        public static void Method3()
        {
            //Point p;
            //p.x = 1;
            //p.y = 2;
            //object obj = p;
            //var res=  obj.GetType();
            //if (!(obj is Point))
            //{
            //}
            //else
            //{ Console.WriteLine("1"); }
            //if (object.ReferenceEquals(p, obj)) { Console.WriteLine("qe"); }



            // The following line generates CS0445, because the result  
            // of unboxing obj is a temporary variable.  
            //((Point)obj).x = 2;

            // The following lines resolve the error.  

            // Store the result of the unboxing conversion in p2.  
            //Point p2;
            //p2 = (Point)obj;
            //// Then you can modify the unboxed value.  
            //p2.x = 2;
            //Point p = new Point();
            //p.x = 2;
            object o;
            //o = p;
            //((IPoint)o).x = 1;
            Stopwatch stopwatch = new Stopwatch();
            Person p1 = new Person();
            p1.Age = 12;
            p1.Name = "neo";
            p1.sex = new Sex("man");
            o = p1;
            //stopwatch.Start();
            

            object pp;
            //stopwatch.Reset();
            stopwatch.Restart();
            pp = p1;
            Person p2 = (Person)pp;
            //((Person)pp).Age = 13;//error
            p2.Age = 13;
            //p2.Name = "fff";
            //p2.sex = new Sex("female");
            stopwatch.Stop();

            Console.WriteLine("耗时：" + stopwatch.ElapsedTicks);


            stopwatch.Restart();
            ((IPersonName)o).Age = 16;
            //((IPersonName)o).Name = "pnp";
            //((IPersonName)o).sex = new Sex("female");
            stopwatch.Stop();
            Console.WriteLine("接口耗时：" + stopwatch.ElapsedTicks);

        }
    }
    public interface IPersonName

    {
        int Age { get; set; }
        string Name { get; set; }
        Sex sex { get; set; }
    }
    public class Sex
    {
        private string _sexName;
        public Sex(string name)
        {
            _sexName = name;
        }
        public string sexName
        {
            get { return _sexName; } 
            set {  _sexName = value; }
        }
       
    }
    public struct Person:IPersonName

    {

        private string _Name;

        public string Name

        {

            get { return _Name; }

            set { _Name = value; }

        }

        public int Age { get; set; }

        public Sex sex { get; set; }


        public override string ToString()

        {

            return _Name;

        }



    }

    // CS0445.CS  

    public interface IPoint

    {

        int x { get; set; }
        int y { get; set; }

    }
    struct Point : IPoint
    {
        //public int x, y;
        public int x { get ; set; }
        public int y { get ; set ; }
    }

}

  

